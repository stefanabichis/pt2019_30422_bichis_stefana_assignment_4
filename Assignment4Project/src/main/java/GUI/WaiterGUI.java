package GUI;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import javax.swing.*;

import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;
import DataLayer.RestaurantSerialization;

public class WaiterGUI extends JFrame {
	private JButton order;
	private JButton bill;
	private JButton view;
	private JTextField idorder;
	private JLabel lblNameProduct;
	private JLabel lblNewLabel;
	private JTextField textField;
	private JLabel lblTable;
	private JTextArea textArea;

	private Restaurant rp = new Restaurant();

	public WaiterGUI() {
		ChefGUI chef = new ChefGUI();
		this.setBounds(255, 255, 718, 384);
		getContentPane().setLayout(null);

		JLabel label = new JLabel("Waiter");
		label.setBounds(309, 16, 87, 77);
		getContentPane().add(label);
		label.setFont(new Font("Tahoma", Font.PLAIN, 30));

		order = new JButton("Create an order");
		order.setBounds(15, 265, 213, 47);
		order.setFont(new Font("Tahoma", Font.PLAIN, 15));
		getContentPane().add(order);
		order.addActionListener(e -> {
			String[] names = this.textArea.getText().split("\n");
			int id = Integer.valueOf(this.idorder.getText());
			int table = Integer.valueOf(this.textField.getText());
			for (int i = 0; i < names.length; i++) {
				MenuItem menuItem = rp.findInMenu(names[i]);
				if (menuItem != null)
					rp.createNewOrder(id, table, names);
				else {
					JFrame frame = new JFrame("ERROR");
					JOptionPane.showMessageDialog(frame, "Items not found");
				}
				break;
			}
			ChefGUI chefpanel = new ChefGUI();
			chefpanel.setVisible(true);
		//	Timer
			chefpanel.label1.setText("Cooking...");
			chefpanel.label1.setVisible(true);
			String s = "";
			for(int i =0; i<names.length;i++) {
				s = s + names[i] + ", ";
			}
			chefpanel.textField.setText(s);

		});

		bill = new JButton("Compute the bill");
		bill.setBounds(246, 265, 213, 47);
		bill.setFont(new Font("Tahoma", Font.PLAIN, 15));

		getContentPane().add(bill);
		bill.addActionListener(e -> {
			int id = Integer.valueOf(this.idorder.getText());
			rp.computeBill(id);

		});

		view = new JButton("View all the orders");
		view.setBounds(474, 265, 213, 47);
		view.setFont(new Font("Tahoma", Font.PLAIN, 15));
		getContentPane().add(view);
		view.addActionListener(e -> {
			JFrame frame = new JFrame();
			JTable table = null;
			String[] header = { "Id", "Table", "Date" };
			HashMap<Order, ArrayList<MenuItem>> orders = RestaurantSerialization.readOrder();
			try {
				table = Table.create(header, orders.keySet());
			} catch (IllegalArgumentException e1) {
				e1.printStackTrace();
			} catch (IllegalAccessException e1) {
				e1.printStackTrace();
			}

			frame.add(table);
			frame.setSize(1000, 500);
			frame.setVisible(true);
		});

		idorder = new JTextField();
		idorder.setBounds(117, 119, 146, 26);
		getContentPane().add(idorder);
		idorder.setColumns(10);

		lblNameProduct = new JLabel("Name product");
		lblNameProduct.setBounds(295, 89, 108, 20);
		getContentPane().add(lblNameProduct);

		lblNewLabel = new JLabel("Id Order");
		lblNewLabel.setBounds(117, 89, 69, 20);
		getContentPane().add(lblNewLabel);

		textField = new JTextField();
		textField.setBounds(458, 119, 146, 26);
		getContentPane().add(textField);
		textField.setColumns(10);

		lblTable = new JLabel("Table");
		lblTable.setBounds(458, 89, 69, 20);
		getContentPane().add(lblTable);

		textArea = new JTextArea();
		textArea.setBounds(295, 127, 131, 122);
		getContentPane().add(textArea);

	}

}
