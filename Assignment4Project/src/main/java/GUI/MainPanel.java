package GUI;

import java.awt.Font;

import javax.swing.*;


public class MainPanel extends JFrame {
	private JButton admin;
	private JButton waiter;
	private JButton chef;
	
	public MainPanel() {
		this.setBounds(255, 255, 718, 384);
		getContentPane().setLayout(null);
		
		JLabel label = new JLabel("Restaurant Management System");
		label.setBounds(140, 16, 448, 77);
		getContentPane().add(label);
		label.setFont(new Font("Tahoma", Font.PLAIN, 30));
		
		admin = new JButton("Administrator");
		admin.setBounds(216, 125, 213, 47);
		admin.setFont(new Font("Tahoma", Font.PLAIN, 15));
		getContentPane().add(admin);
		admin.addActionListener(e -> {
			AdminGUI adminpanel = new AdminGUI();
			adminpanel.setVisible(true);
		});


		waiter = new JButton("Waiter");
		waiter.setBounds(216, 188, 213, 47);
		waiter.setFont(new Font("Tahoma", Font.PLAIN, 15));
		getContentPane().add(waiter);
		waiter.addActionListener(e -> {
			WaiterGUI waiterpanel = new WaiterGUI();
			waiterpanel.setVisible(true);
		});
		
		chef = new JButton("Chef");
		chef.setBounds(216, 251, 213, 47);
		chef.setFont(new Font("Tahoma", Font.PLAIN, 15));
		getContentPane().add(chef);
		chef.addActionListener(e -> {
			ChefGUI chefpanel = new ChefGUI();
			chefpanel.setVisible(true);
		});
	}
}
