package GUI;

import java.awt.*;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;

import javax.swing.*;
import BusinessLayer.MenuItem;
import BusinessLayer.Restaurant;
import BusinessLayer.RestaurantProcessing;
import DataLayer.RestaurantSerialization;




public class AdminGUI extends JFrame {
	
	
	private JButton add;
	private JButton delete;
	private JButton modify;
	private JButton view;
	private JTextField nameCompProd;
	private JTextField removeByName;
	private JTextField modifyName;
	private JTextField modifyPrice;
	private JTextArea name; 
	private JTextArea price; 
	private JCheckBox chckbxCompositeproduct;
	
	private Restaurant rp = new Restaurant();
	
	public String getNames() {
		return this.name.getText();
	}
	
	
	public AdminGUI() {
	this.setBounds(255, 255, 915, 484);
	getContentPane().setLayout(null);
	
	JLabel label = new JLabel("Administrator");
	label.setBounds(331, 16, 183, 77);
	getContentPane().add(label);
	label.setFont(new Font("Tahoma", Font.PLAIN, 30));
	
	add = new JButton("Add a product");
	add.setBounds(359, 236, 213, 47);
	add.setFont(new Font("Tahoma", Font.PLAIN, 15));
	getContentPane().add(add);
	add.addActionListener(e->{
		String[] names = this.name.getText().split("\n");
		String[] prices_strings = this.price.getText().split("\n");
		float[] prices = new float[prices_strings.length];
		for(int i=0; i<prices_strings.length; i++) {
			prices[i]=Float.parseFloat(prices_strings[i]);
		}
		
		if(this.chckbxCompositeproduct.isSelected()) {
			rp.addMenuItem(names, prices, this.chckbxCompositeproduct.isSelected(), this.nameCompProd.getText());
		}
		else
		{
			rp.addMenuItem(names, prices, this.chckbxCompositeproduct.isSelected(),"");
		}
		
	});
	


	delete = new JButton("Delete a product");
	delete.setBounds(616, 157, 213, 47);
	delete.setFont(new Font("Tahoma", Font.PLAIN, 15));
	getContentPane().add(delete);
	delete.addActionListener(e->{
			String name = this.removeByName.getText();
			rp.removeMenuItem(name);
		
		
	});

	
	modify = new JButton("Modify a product");
	modify.setBounds(665, 365, 213, 47);
	modify.setFont(new Font("Tahoma", Font.PLAIN, 15));
	getContentPane().add(modify);
	modify.addActionListener(e->{
		String name = this.modifyName.getText();
		float price = Float.parseFloat(this.modifyPrice.getText());
		rp.modifyMenuItem(name,price);
	
	
});
	
	view = new JButton("View all the products");
	view.setBounds(25, 343, 213, 47);
	view.setFont(new Font("Tahoma", Font.PLAIN, 15));
	getContentPane().add(view);
	view.addActionListener(e -> {
		JFrame frame = new JFrame();
		JTable table = null;
		String[] header = {"Name", "Price"}; 
		HashSet<MenuItem> products = RestaurantSerialization.readMenu();
			try {
				table = Table.create(header, products);
			} catch (IllegalArgumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IllegalAccessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
		frame.add(table);
		frame.setSize(1000,500);
		//frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	});
	
	
	JLabel lblName = new JLabel("Name");
	lblName.setBounds(25, 96, 69, 20);
	getContentPane().add(lblName);
	
	JLabel lblPrice = new JLabel("Price");
	lblPrice.setBounds(192, 96, 69, 20);
	getContentPane().add(lblPrice);
	
	name = new JTextArea();
	name.setBounds(25, 126, 152, 186);
	getContentPane().add(name);
	
	price = new JTextArea();
	price.setBounds(192, 126, 152, 186);
	getContentPane().add(price);
	
	chckbxCompositeproduct = new JCheckBox("CompositeProduct");
	chckbxCompositeproduct.setBounds(355, 117, 172, 29);
	chckbxCompositeproduct.setSelected(false);
	getContentPane().add(chckbxCompositeproduct);
	
	nameCompProd = new JTextField();
	nameCompProd.setBounds(359, 194, 146, 26);
	getContentPane().add(nameCompProd);
	nameCompProd.setColumns(10);
	
	JLabel nameCompositeProd = new JLabel("Name Composite Product");
	nameCompositeProd.setBounds(359, 158, 190, 20);
	getContentPane().add(nameCompositeProd);
	
	removeByName = new JTextField();
	removeByName.setBounds(616, 118, 146, 26);
	getContentPane().add(removeByName);
	removeByName.setColumns(10);
	
	JLabel remByName = new JLabel("Remove by name");
	remByName.setBounds(616, 96, 122, 20);
	getContentPane().add(remByName);
	
	modifyName = new JTextField();
	modifyName.setBounds(571, 323, 146, 26);
	getContentPane().add(modifyName);
	modifyName.setColumns(10);
	
	modifyPrice = new JTextField();
	modifyPrice.setBounds(732, 323, 146, 26);
	getContentPane().add(modifyPrice);
	modifyPrice.setColumns(10);
	
	JLabel modName = new JLabel("Name");
	modName.setBounds(573, 292, 69, 20);
	getContentPane().add(modName);
	
	JLabel modPrice = new JLabel("Price");
	modPrice.setBounds(734, 292, 69, 20);
	getContentPane().add(modPrice);


	}
}
