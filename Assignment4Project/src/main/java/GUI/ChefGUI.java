package GUI;

import java.awt.Font;
import java.util.Observable;
import java.util.Observer;

import javax.swing.*;

public class ChefGUI extends JFrame {
	JTextField textField;
	JLabel label;
	JLabel label1;

	public ChefGUI() {
		this.setBounds(255, 255, 718, 384);
		getContentPane().setLayout(null);

		label = new JLabel("Chef");
		label1 = new JLabel("Waiting");
		label.setBounds(319, 16, 75, 77);
		label1.setBounds(319, 100, 75, 77);
		getContentPane().add(label);
		getContentPane().add(label1);
		label.setFont(new Font("Tahoma", Font.PLAIN, 30));

		textField = new JTextField();
		textField.setBounds(175, 165, 340, 26);
		getContentPane().add(textField);
		textField.setColumns(10);

	}

}
