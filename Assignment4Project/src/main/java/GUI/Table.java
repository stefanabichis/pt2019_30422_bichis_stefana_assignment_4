package GUI;
import java.lang.reflect.Field;
import java.util.*;
import javax.swing.*;
import javax.swing.table.*;
import java.lang.Object;

import BusinessLayer.Order;

public class Table {
	public static <T> JTable create(String[] header, Set<T> list) throws IllegalArgumentException, IllegalAccessException {
		DefaultTableModel model = new DefaultTableModel();
		model.setColumnIdentifiers(header);
		Object[][] data = (T[][]) new Object[list.size()][header.length];
			int line=0,column=0;
			for(Object o: list) {
				Field[] fields;
				if(o.getClass()==Order.class) {
					fields = o.getClass().getDeclaredFields();
				}
				else {
					fields = o.getClass().getSuperclass().getDeclaredFields();
				}
				column=0;
				for(Field f : fields) {
					f.setAccessible(true);
					Object value;
					try {
						value = (Object) f.get(o);
						data[line][column]=value;
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
					column++;
				}
				line++;
			}
			return new JTable(data,header);
		}
}
