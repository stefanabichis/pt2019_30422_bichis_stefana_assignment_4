package BusinessLayer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Observable;
import java.util.Observer;

import BusinessLayer.*;
import DataLayer.FileWriter;
import DataLayer.RestaurantSerialization;

public class Restaurant extends Observable implements RestaurantProcessing{
	private static HashSet<MenuItem> items;
	private static HashMap<Order, ArrayList<MenuItem>> orders;

	public Restaurant() {
		this.items = new HashSet<MenuItem>();
		this.orders = new HashMap<Order, ArrayList<MenuItem>>();
	}

	
	public MenuItem findInMenu(String name) {
		HashSet<MenuItem> menuItems = RestaurantSerialization.readMenu();
		MenuItem menuItem = null;
		for(MenuItem m: menuItems) {
			if(m.getName().equals(name)) {
				menuItem = m;
			}
		}
		return menuItem;
	}

	public static HashSet<MenuItem> getItems() {
		return items;
	}

	public static void setItems(HashSet<MenuItem> items) {
		Restaurant.items = items;
	}

	public static HashMap<Order, ArrayList<MenuItem>> getOrders() {
		return orders;
	}

	public static void setOrders(HashMap<Order, ArrayList<MenuItem>> orders) {
		Restaurant.orders = orders;
	}

	// admin
	public void addMenuItem(String[] name, float[] price, boolean compProd, String compName) {
		HashSet<MenuItem> menuItems = RestaurantSerialization.readMenu();

		if (compProd == false) {
			MenuItem m = new BaseProduct(name[0], price[0]);
			menuItems.add(m);
		} else {

			CompositeProduct m = new CompositeProduct(compName);
			for (int i = 0; i < name.length; i++) {
				MenuItem menuItem = new BaseProduct(name[i], price[i]);
				m.getProducts().add(menuItem);
			}
			m.setPrice(m.generatePrice());
			menuItems.add(m);
		}
		System.out.println(menuItems);
		RestaurantSerialization.writeMenu(menuItems);
	}

	public void removeMenuItem(String name) {
		HashSet<MenuItem> menuItems = RestaurantSerialization.readMenu();
		for (MenuItem m : menuItems) {
			if (m.getName().equals(name)) {
				menuItems.remove(m);
				break;
			}
		}
		System.out.println(menuItems);
		RestaurantSerialization.writeMenu(menuItems);
	}

	public void modifyMenuItem(String name, float price) {
		HashSet<MenuItem> menuItems = RestaurantSerialization.readMenu();
		for (MenuItem m : menuItems) {
			if (m.getName().equals(name)) {
				m.setPrice(price);
				break;
			}
		}
		System.out.println(menuItems);
		RestaurantSerialization.writeMenu(menuItems);

	}

	// waiter
	public void createNewOrder(int id, int table, String[] name) {
		HashSet<MenuItem> menuItems = RestaurantSerialization.readMenu();
		HashMap<Order, ArrayList<MenuItem>> orderItems = RestaurantSerialization.readOrder();
		ArrayList<MenuItem> list = new ArrayList<MenuItem>();
		// Date date = new Date();
		// Order order = new Order(id, table, date);
		for (int i = 0; i < name.length; i++) {
			for (MenuItem m : menuItems) {
				if (m.getName().equals(name[i])) {
					list.add(m);
					break;
				}
			}
		}
		Date date = new Date();
		Order order = new Order(id, table, date);
		orderItems.put(order, new ArrayList<MenuItem>());
		orderItems.get(order).addAll(list);

		RestaurantSerialization.writeOrder(orderItems);
	}

	public void computeBill(int orderid) {
		HashMap<Order, ArrayList<MenuItem>> orderItems = RestaurantSerialization.readOrder();
		float sum = 0;
		for (Order o : orderItems.keySet()) {
			if (o.getId() == orderid) {
				for (MenuItem m : orderItems.get(o)) {
					System.out.println(m.getPrice());
					sum = sum + m.getPrice();
				}
				break;
			}
		}
		String bill = "Order " + orderid + " \n" + "TOTAL: " + sum;
		PrintWriter print = null;
		try {
			print = new PrintWriter("bill.txt", "UTF-8");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		print.print(bill);
		print.close();
		System.out.println("Bill created");
	}




}
