package BusinessLayer;

import java.io.*;
import java.util.*;

public abstract class MenuItem implements Serializable {
	private String name;
	private float price;

	public MenuItem() {

	}
	public MenuItem(String name) {
		this.setName(name);
		this.setPrice(0f);
	}
	public MenuItem(String name, float price) {
		this.setName(name);
		this.setPrice(price);
	}

	public String toString() {
		return "Product name: " + name + "     Price: " + price + "\n";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MenuItem other = (MenuItem) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Float.floatToIntBits(price) != Float.floatToIntBits(other.price))
			return false;
		return true;
	}

	public abstract float generatePrice();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

}
