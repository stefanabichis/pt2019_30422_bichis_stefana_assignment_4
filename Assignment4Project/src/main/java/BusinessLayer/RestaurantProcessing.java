package BusinessLayer;

import java.io.Serializable;
import java.util.ArrayList;



public interface RestaurantProcessing {

	//admin
	public void addMenuItem(String[] name, float[] price, boolean compProd, String compName);
	public void removeMenuItem(String name);
	public void modifyMenuItem(String name, float price);
	
	//waiter
	public void createNewOrder(int id, int table, String[] name);
	public void computeBill(int orderid);
		
}
