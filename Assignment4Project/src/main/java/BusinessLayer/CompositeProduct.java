package BusinessLayer;

import java.util.ArrayList;
import java.io.*;

public class CompositeProduct extends MenuItem {
	private String name;
	private float price;
	private ArrayList<MenuItem> products;

	public CompositeProduct(String name) {
		super(name);
		this.products = new ArrayList<MenuItem>();
	}
	public CompositeProduct(String name, float price) {
		super(name, price);
		this.products = new ArrayList<MenuItem>();
	}

	public void addItem(MenuItem menuItem) {
		this.products.add(menuItem);
	}

	public void removeItem(MenuItem menuItem) {
		this.products.remove(menuItem);
	}
	
	public ArrayList<MenuItem> getProducts(){
		return this.products;
	}

	@Override
	public float generatePrice() {
		float price = 0;
		for (MenuItem m : this.products) {
			price = price + m.getPrice();
		}
		return price;
	}

}
