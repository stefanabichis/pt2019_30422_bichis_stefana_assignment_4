package DataLayer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import BusinessLayer.MenuItem;
import BusinessLayer.Order;

public class RestaurantSerialization {

	public static void writeMenu(HashSet<MenuItem> items) {
		try (ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream("menu.ser"))) {
			stream.writeObject(items);
		} catch (FileNotFoundException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		}
	}

	public static void writeOrder(HashMap<Order, ArrayList<MenuItem>> orders) {
		try (ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream("order.ser"))) {
			stream.writeObject(orders);
		} catch (FileNotFoundException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		}
	}

	public static HashSet<MenuItem> readMenu() {
		try (ObjectInputStream stream = new ObjectInputStream(new FileInputStream("menu.ser"))) {
			HashSet<MenuItem> menu = (HashSet<MenuItem>) stream.readObject();
			return menu;
		} catch (FileNotFoundException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();

		}
		return null;
	}

	public static HashMap<Order, ArrayList<MenuItem>> readOrder() {
		try (ObjectInputStream stream = new ObjectInputStream(new FileInputStream("order.ser"))) {
			HashMap<Order, ArrayList<MenuItem>> order = (HashMap<Order, ArrayList<MenuItem>>) stream.readObject();
			return order;
		} catch (FileNotFoundException e) {
			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();

		}
		return null;

	}

}
