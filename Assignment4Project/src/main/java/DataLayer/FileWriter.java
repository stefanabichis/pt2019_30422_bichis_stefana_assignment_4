package DataLayer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;

import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;

public class FileWriter {
	Restaurant restaurant;
	
	
	public FileWriter(Restaurant restaurant, Order order) {
		this.restaurant = restaurant;
		PrintWriter print = null;
		File file = new File("Order " + order.getId());
		float price = 0;
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			print = new PrintWriter(file);
			print.println("Order: " + order.getId());
			print.println("Table: " + order.getTable());
			print.println("Products: ");
			HashSet<MenuItem> orderitems = RestaurantSerialization.readMenu();
			for (MenuItem i : orderitems) {
				
				price = price + i.getPrice();
				print.println(i.getName() + ", price: " + i.getPrice());
			}
			print.println("TOTAL: " + price);
			print.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
