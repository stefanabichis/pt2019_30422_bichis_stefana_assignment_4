package PT2019.assignment4.Assignment4Project;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import BusinessLayer.BaseProduct;
import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;
import DataLayer.RestaurantSerialization;
import GUI.MainPanel;

public class App {
	private static HashSet<MenuItem> items;
	private static HashMap<Order, ArrayList<MenuItem>> orders;

	public static void main(String[] args) {
		/*Restaurant r = new Restaurant();

		MenuItem m1 = new BaseProduct("Pizza", 20.0f);
		MenuItem m2 = new BaseProduct("Salam", 10.0f);

		Date date = new Date();
		Order o1 = new Order(1, 3, date);
		Order o2 = new Order(2, 4, date);

		items.add(m1);
		items.add(m2);
		RestaurantSerialization.writeMenu(items);
		HashSet<MenuItem> results = RestaurantSerialization.readMenu();
		System.out.println(results);

		// orders.put(o1, new ArrayList<MenuItem>());
		// orders.put(o2, new ArrayList<MenuItem>());

		orders.put(o1, new ArrayList<MenuItem>());
		// System.out.println(orders);
		orders.put(o2, new ArrayList<MenuItem>());
		// System.out.println(orders);
		orders.get(o1).add(m1);
		// System.out.println(orders);
		orders.get(o2).add(m1);
		// System.out.println(orders);
		RestaurantSerialization.writeOrder(orders);
		HashMap<Order, ArrayList<MenuItem>> resultsOrder = RestaurantSerialization.readOrder();
		System.out.println(orders);

		items.add(m1);
		System.out.println(m1.hashCode());
		System.out.println(m2.hashCode());
		System.out.println(m1.equals(m2));
		System.out.println(items);
		items.add(m2);
		System.out.println(items);*/

		MainPanel mainPanel = new MainPanel();
		mainPanel.setVisible(true);
	}
}
